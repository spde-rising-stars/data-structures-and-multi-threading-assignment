package com.practice;
class MyThread extends Thread
{ 
	public void run()
	{
		for(int i=0;i<10;i++)
		{
			System.out.println("Child Thread");
			try
			{
				Thread.sleep(2000);
			}
			catch(InterruptedException e) {}
	}
}
}
public class AssQ4 {

	public static void main(String[] args) {
		System.out.println(Thread.currentThread().isDaemon());
		//Thread.currentThread().setDaemon(true);
		MyThread t = new MyThread();
		System.out.println(t.isDaemon());
		t.setDaemon(true);
		System.out.println(t.isDaemon());
		t.start();
		System.out.println("end of main thread");
	}
		

}
	
