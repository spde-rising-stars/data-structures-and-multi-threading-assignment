package com.practice;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Producer extends Thread{
	private static final int MAX_SIZE =3;
	 private  final List<String> messages = new ArrayList<>();
	 @Override
	 public void run() {
		 try {
			     while(true) {
			    	 produce();
			     } 
		 }catch (Exception exp) {}
	   }
	 

	 

	private synchronized void produce() throws  Exception {
		while(messages.size() == MAX_SIZE){
		    System.out.println("Queue limit.waiting for consumer");
		wait();
		
		}
		String data = LocalDate.now().toString();
		messages.add(data);
		System.out.println("Producer produced date");
		notify();
		System.out.println("Producer got notification from consumer");
		}

	public synchronized  String consume() throws Exception {
		notify();
		while(messages.isEmpty())
		{
			while(messages.isEmpty()) {
				wait();
			}
		}
			String data = messages.get(0);
			 messages.remove(data);
			 return data;
		
	}
	}

	
	
	    
		
	


	

