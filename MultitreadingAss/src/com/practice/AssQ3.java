package com.practice;
 class ThreadDemo extends Thread {
	Thread t;
	String name ;
	int priority;
	ThreadDemo(int priority ,String name)
	{
		this.name = name;
		this.priority = priority;
		t = new Thread(this,name);
		System.out.println("New Thread" +t);
		t.start();
	}
	public void run()
	{
		int p=t.getPriority();
		try
		{
			if(p==9||p==8)
			{
				t.sleep(2000);
			}
			else
			{
				System.out.println(t.currentThread()+"is alive?"+t.isAlive());
				
			}
			int count=0;
			while(t.isAlive() && count!=4)
			{
				System.out.println("Long lasting thread:" +t.getName());
				count++;
			}
		}
		catch(InterruptedException ie)
		{
			System.out.println("Thread Interrupted..!");
		}
	}
}
public class AssQ3 {

	public static void main(String[] args) {
		new ThreadDemo(5 ,"one");
		new ThreadDemo(6 ,"two");
		new ThreadDemo(7,"Three");
		new ThreadDemo(8 ,"Four");
		new ThreadDemo(9 ,"Five");
	}
	

}
