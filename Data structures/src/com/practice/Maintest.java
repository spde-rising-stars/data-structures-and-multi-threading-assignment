package com.practice;


public class Maintest {

	public static void main(String[] args) {
		EmployeeDB empDb = new EmployeeDB();

		Employee emp1 = new Employee(101, "sushmitha", "suthangellapally@gmail.com", 'F', 25000);
		Employee emp2 = new Employee(102, "srilakshmi", "srang@gmail.com", 'F', 20000);
		Employee emp3 = new Employee(103, "saidivya",  "sgondr@gmail.com",'F', 20000);
		Employee emp4 = new Employee(104, "Prathyusha", "psale@gmail.com", 'F', 20000);

		empDb.addEmployee(emp1);
		empDb.addEmployee(emp2);
		empDb.addEmployee(emp3);
		empDb.addEmployee(emp4);

		for (Employee emp : empDb.listAll())
			System.out.println(emp.GetEmployeeDetails());

		System.out.println();
		empDb.deleteEmployee(102);

		for (Employee emp : empDb.listAll())
			System.out.println(emp.GetEmployeeDetails());

		System.out.println();

		System.out.println(empDb.showPaySlip(103));
	}



	}


